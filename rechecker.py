from flask import Flask, Response, request, jsonify, abort
import requests
import dns
import dns.resolver
import os
import json
import sys
import pymongo
from pymongo import MongoClient
from slack_webhook import Slack
from random import randint

# set system variables
SERVER_NAME = "API/0.1.0"
#ABSTRACT_API_KEY = os.environ["ABSTRACT_API_KEY"]

# class to hide http headers
class localFlask(Flask):
    def process_response(self, response):
        response.headers["server"] = SERVER_NAME
        return response

# set flask variables
app = localFlask(__name__)

# authorization layer with abstract_api_key variable
# @app.before_request
# def before_request():
#     if 'ABSTRACT_API_KEY' not in request.headers or request.headers.get("ABSTRACT_API_KEY") != ABSTRACT_API_KEY:
#         return jsonify({"message": "ERROR: Unauthorized"}), 401

# test root for app up
@app.route("/", methods=["GET"])
def health():
    return jsonify({"health": "ok"}), 200

# set global variables, lists, and dictionaries
website = []
http_timed_out = []
failed_dns_checker = []
failed_http_checker = {}
attempts_time_out = []
attempts_dns_check = []
attempts_http_check = {}

slack = Slack(url='https://hooks.slack.com/url/for/webhook')

def get_endpoints():
    print('I provide a list of endpoints')
    try:
        client = MongoClient("mongodb+srv://user:password@cluster.id.mongodb.net/host?retryWrites=true&w=majority")
        # .hosts refers to the database to connect to
        db = client.hosts
        # This for look loop iterates over each entry in the database.
        # These are json entries with name: "domain.com" key value pairs
        # .sites is the Collection in the .hosts database
        for web in db.sites.find():
            global website
            # appending each domain found to website global variable
            website.append(web['name'])
        return jsonify({"collecting endpoints": "ok"}), 200    
    # in case URI syntax is incorrect
    except pymongo.errors.ConfigurationError:
        print('Please check the syntax of your URI in MongoClient')
        return abort(404)
    except pymongo.errors.InvalidURI:
        print('Please check the syntax of your URI in MongoClient')
        return abort(401)
    # in case username or password is incorrect
    except pymongo.errors.OperationFailure:
        print('Check your username and password')
        return abort(401)

def http_checker():
    print('I check http status')
    for site in website:
        #global failed_http_checker
        global failed_http_checker
        try:
            # Sets timeout to 2 seconds
            call = requests.get('http://' + site, timeout=2)
            if call.status_code == 200:
                print('The response code is ' + str(call.status_code) + ' for ' + site)
            elif call.status_code == 302:
                print('The response code is ' + str(call.status_code) + ' for ' + site)
            else:
                print('http status failed for ' + site + ' with status code ' + str(call.status_code))
                failed_http_checker.update( { site: str(call.status_code) } )
        # If timeout fails appends to failed_http_checker variable
        except requests.ConnectionError:
            print('http request timedout after 2 seconds for ' + site)
            attempts_time_out.append(site)
            #print(attempts_time_out)
            #print(website)
            global http_timed_out
            if site in http_timed_out:
                pass
            else:
                http_timed_out.append(site)
    pass

def dns_checker():
    print('I check DNS status')
    global failed_dns_checker
    for site in website:
        try:
            call = dns.resolver.resolve(site, 'A')
            for ipval in call:
                print('IP', site, ipval.to_text())
        # Currently only catching if domain not found
        # Think of adding port closed or down as well
        # Also appends to failed_dns_checker variable
        except dns.resolver.NXDOMAIN:
            print('Name record not found for ', site)
            failed_dns_checker.append(site)
        except dns.resolver.NoAnswer:
            print('No reply for name record ', site)
            failed_dns_checker.append(site)
    pass

def double_checker():
    print('I double check any sites in a failed state')
    for site in attempts_time_out:
        if attempts_time_out.count(site) > 2:
            #print("This site needs to be addressed for timeouts " + site)
            break
        else:
            global http_timed_out
            global website
            website = http_timed_out
            http_checker()
    pass
def notify_slack():
    print('I notify slack')
    # To minimize overhead I've added an or statement to both slack and email notifier
    # Ideally I'd like to catch both http and dns rather than on a first come basis
    # but i'm leaving for now
    for site in http_timed_out:
        print("notifying slack channel that " + site + " has failed timeout")
        slack.post(text='Website ' + site + ' Timed out on http check')
    for site,code in failed_http_checker.items():
        print("notifying slack channel that " + site + " has failed http check")
        slack.post(text='Website ' + site + ' Failed with http code ' + code)
    for site in failed_dns_checker:
        print("notifying slack channel that " + site + " has failed dns check")
        slack.post(text='Website ' + site + ' Failed dns check')
    pass

def notify_email():
    print('I send an email http status')
    for site in http_timed_out:
        print("notifying email list that " + site + " has failed timeout")
    for site,code in failed_http_checker.items():
        print("notifying email list that " + site + " has failed http check")
    for site in failed_dns_checker:
        print("notifying email list that " + site + " has failed dns check")
    pass

@app.route("/mongodb", methods=["GET"])
def endgame():
    get_endpoints()
    http_checker()
    dns_checker()
    double_checker()
    notify_slack()
    notify_email()
    return jsonify({"healthcheck": "ran"}), 200
#________________________________________________________

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5001')
